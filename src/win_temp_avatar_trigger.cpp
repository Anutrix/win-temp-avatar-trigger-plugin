#include "win_temp_avatar_trigger.h"
#include <godot_cpp/core/class_db.hpp>

using namespace godot;

void WinTempAvatarTrigger::_bind_methods() {
}

WinTempAvatarTrigger::WinTempAvatarTrigger() {
    // Initialize any variables here.
}

WinTempAvatarTrigger::~WinTempAvatarTrigger() {
    // Add your cleanup here.
}

#if defined(_WIN32)
void WinTempAvatarTrigger::_ready() {
    getWinTempAvatarPath();
}

typedef HRESULT(WINAPI *pfnSHGetUserPicturePathEx)(
    LPCWSTR pwszUserOrPicName,
    DWORD sguppFlags,
    LPCWSTR pwszDesiredSrcExt,
    LPWSTR pwszPicPath,
    UINT picPathLen,
    LPWSTR pwszSrcPath,
    UINT srcLen);

void WinTempAvatarTrigger::getWinTempAvatarPath() {
    HMODULE hMod = LoadLibrary("shell32.dll");
    pfnSHGetUserPicturePathEx picPathFn = (pfnSHGetUserPicturePathEx)GetProcAddress(hMod, (LPCSTR)810);
    WCHAR picPath[500] = {0}, srcPath[500] = {0};
    picPathFn(NULL, 0, NULL, picPath, ARRAYSIZE(picPath), srcPath, ARRAYSIZE(srcPath));
}
#endif
