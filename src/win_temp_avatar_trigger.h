#ifndef WINTEMPAVATARTRIGGER_H
#define WINTEMPAVATARTRIGGER_H

#include <godot_cpp/classes/node.hpp>

#if defined(_WIN32)
#include <windows.h>
#endif

namespace godot {

class WinTempAvatarTrigger : public Node {
    GDCLASS(WinTempAvatarTrigger, Node)

#if defined(_WIN32)
  private:
    void getWinTempAvatarPath();
#endif

  protected:
    static void _bind_methods();

  public:
    WinTempAvatarTrigger();
    ~WinTempAvatarTrigger();

#if defined(_WIN32)
    void _ready() override;
#endif
};
} // namespace godot

#endif
