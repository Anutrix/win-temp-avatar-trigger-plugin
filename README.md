# win_temp_avatar_trigger
WinTempAvatarTrigger, a simple Godot 4 plugin which provides a simple node that triggers a Windows 8, 8.1, 10 and 11 hack for temporary generation of User Account's Avatar or Profile Picture.
Full path to access it is `%LOCALAPPDATA%\\Temp\\<user_name>.bmp`.

**Note:** This project currently works with Godot master build 89850d553 not stable version. Also, it's a debug build.

**Usage:**
 - For setup, compilation as needed, first follow https://docs.godotengine.org/en/latest/tutorials/scripting/gdextension/gdextension_cpp_example.html with appropriate name changes if needed. If you don't plan to make changes to plugin code, this setup is one-time action.
 - Then copy `bin` folder inside demo folder and paste directly into your Godot 4 project.
 - Open your Godot Project and add new node called `WinTempAvatarTrigger` in any single scene.
 - OR Add following snippet in `_ready()` function in any one of your scripts(preferably something that runs early and once):
```
if os_name == "Windows" or os_name == "UWP":
    add_child(WinTempAvatarTrigger.new())
```

**Note:** Since there's no way ifdef in GDScript currently, a dummy class/node is used if we need to export to other platforms like Android.
